const mongoose = require("mongoose")

const DelCards = new mongoose.Schema({
  type: Number,
  name: String,
  MAHE_price: String,
  Non_price: String,
  active: Boolean,
});

module.exports = DelCards1 = mongoose.model("DelCards", DelCards);