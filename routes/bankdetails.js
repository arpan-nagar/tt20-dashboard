const express = require('express')
const User = require('../models/User');
const BankDetails = require('../models/BankDetails');

let exp = {};

exp.addBankDetails = async(req,res) =>{
    try{
        const userID = req.session.userID;
        const {holderName,accountNo,ifsc,bankName,branchName} = req.body;

        const details = await BankDetails.findOne({userID});

        if(details){
            await BankDetails.deleteOne({userID});
        }

        const bankDetails = new BankDetails({
            userID,
            holderName,
            accountNo,
            ifsc,
            bankName,
            branchName
        });

        await bankDetails.save();

        return res.send({ success: true, msg: "Updated Bank Details Successfully" });
    }
    catch(err){
        console.log(err);
        return res.send({ success: false, msg: "Internal Server Error" });
    }
}

module.exports = exp;