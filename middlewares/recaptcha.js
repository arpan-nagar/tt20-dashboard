const axios = require('axios');

module.exports = (req, res, next) => {

    if(
        req.body.v1 === undefined ||
        req.body.v1 === '' ||
        req.body.v1 === null
    ){
        return res.send({success: false,msg: 'Please Select Captcha'});
    }

    const secretKey = process.env.CAPTCHA_SECRET_KEY;

    const verifyUrl = `https://www.google.com/recaptcha/api/siteverify?secret=${secretKey}&response=${req.body.v1}&remoteip=${req.connection.remoteAddress}`

    axios.post(verifyUrl)
    .then((response) => {
        if(response.data.success !== undefined && !response.data.success){
            return res.send({success: false,msg: 'Please try again! Captcha failed'});
        }
        next();
    })
    .catch((err) => {
        console.log(err.toString());
        return res.send({success: false,msg: 'Please try again! Captcha failed'});
    })

  };