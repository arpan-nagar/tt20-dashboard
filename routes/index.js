const express = require("express");
const router = express.Router();
const register = require("./register");
const bankDetails = require("./bankdetails");
const paytm = require("./paytm");
const axios = require("axios");
const events = require("./events");
const app = require("./app");
const User = require("../models/User");
const Event = require("../models/Event");
const BankDetails = require('../models/BankDetails');

const isVerified = async (req, res, next) => {
  const userID = req.session.userID;
  const user = await User.findOne({ userID });

  if (!user) {
    return res.send({ success: false, msg: "User not found." });
  }

  if (user.verified == "UNVERIFIED") {
    return res.send({ success: false, msg: "You have not been verified yet." });
  }

  if (user.verified == "REJECTED") {
    return res.send({
      success: false,
      msg: "User registration rejected. Contact System Admin.",
    });
  }
  next();
};

const accessCheck = async (req, res, next) => {
  const userID = req.session.userID;

  const { eventID } = req.body;

  const event = await Event.findOne({ eventID });

  if (!event) {
    return res.send({ success: false, msg: "Event not found." });
  }

  if (event.eventType === "Pre TechTatva") {
    next();
  } else {
    const user = await User.findOne({ userID });

    if (!user) {
      return res.send({ success: false, msg: "User not found." });
    }

    if (user.verified == "UNVERIFIED") {
      return res.send({
        success: false,
        msg: "You have not been verified yet.",
      });
    }

    if (user.verified == "REJECTED") {
      return res.send({
        success: false,
        msg: "User registration rejected. Contact System Admin.",
      });
    }
    next();
  }
};

const captchaVerify = async (req, res, next) => {
  if (
    req.body.captcha === undefined ||
    req.body.captcha === "" ||
    req.body.captcha === null
  ) {
    return res.send({ success: false, msg: "Please Select Captcha" });
  }
  let userID = req.session.userID;
  const secretKey = process.env.CAPTCHA_SECRET_KEY;

  const verifyUrl = `https://www.google.com/recaptcha/api/siteverify?secret=${secretKey}&response=${req.body.captcha}&remoteip=${req.connection.remoteAddress}`;

  axios
    .post(verifyUrl)
    .then((response) => {
      if (response.data.success !== undefined && !response.data.success) {
        return res.send({
          success: false,
          msg: "Please try again! Captcha failed",
        });
      }
      next();
    })
    .catch((err) => {
      console.log(err.toString());
      return res.send({
        success: false,
        msg: "Please try again! Captcha failed",
      });
    });
};

const isLoggedIn = async (req, res, next) => {
  if (req.session.logged_in) {
    next();
  } else res.redirect("/login");
};

// frontend
router.get("/", async (req, res) => {
  let login = false;
  if (req.session.userID) login = true;
  res.render("index", { login });
});

router.get("/login", async (req, res) => {
  if (req.session.logged_in) res.redirect("/events");
  else res.render("login");
});

router.get("/loginNew", async (req, res) => {
  if (req.session.logged_in) res.redirect("/");
  else res.render("loginNew");
});

router.get("/pay", register.hasAuth, (req, res) => {
  res.render("paytm");
});

router.get("/forgotpass", (req, res) => {
  res.render("forgotpassword1");
});

router.get("/tshirts", (req, res) => {
  res.render("tshirts");
});

router.get("/gallery", (req, res) => {
  res.render("gallery");
});

router.get("/conveners", isLoggedIn, async (req, res) => {
  const user = await User.findOne({ userID: req.session.userID });
  res.render("conveners", {
    userID: user.userID,
    driveLink: user.driveLink,
    verified: user.verified,
  });
});

router.get("/updatedrive", isLoggedIn, async (req, res) => {
  const user = await User.findOne({ userID: req.session.userID });

  res.render("updateDrive", {
    userID: user.userID,
    driveLink: user.driveLink,
    verified: user.verified,
  });
});

router.get("/bankdetails", isLoggedIn, async (req, res) => {
  const user = await User.findOne({ userID: req.session.userID });
  let details = await BankDetails.findOne({ userID: req.session.userID});

  if(!details){
    details = {
      holderName:'',
      accountNo:'',
      ifsc:'',
      bankName:'',
      branchName:''
    }
  }

  res.render("bankdetails", {
    userID: user.userID,
    verified: user.verified,
    holderName:details.holderName,
    accountNo:details.accountNo,
    ifsc:details.ifsc,
    bankName:details.bankName,
    branchName:details.branchName
  });
});

router.get("/events", isLoggedIn, events.fullevents);

router.get("/results", isLoggedIn, events.results);
router.get("/results/:id", isLoggedIn, events.singleResult);

router.get("/registered", isLoggedIn, events.registered);

router.get("/events/:id", isLoggedIn, events.singleEvent);
router.get("/registered/:eventID/:teamID", isLoggedIn, events.regEvent);

router.get("/changepass/:id", (req, res) => {
  console.log(req.params.id);
  res.render("changepass", {
    token: req.params.id,
  });
});

router.get("/filterevents", isLoggedIn, events.filter);

//auth+registration+fp routes
router.get("/api/status", isLoggedIn, register.status);
router.get("/api/logout", isLoggedIn, register.logout);

router.post("/api/signup", captchaVerify, register.register);
router.post("/api/login", captchaVerify, register.login);
router.post("/api/forgotpass", register.forgotPassword);
router.post("/api/changepass/:id", register.checkPassword);
router.post("/api/updatedrive", isLoggedIn, events.updateDrive);

// event registration routes
router.post(
  "/api/registerevent",
  isLoggedIn,
  accessCheck,
  events.registerEvent,
);
router.post("/api/createteam", isLoggedIn, accessCheck, events.createTeam);
router.post("/api/jointeam", isLoggedIn, accessCheck, events.joinTeam);

router.post("/api/leaveteam", events.leaveTeam);
router.post("/api/removeuser", isLoggedIn, accessCheck, events.removeUser);

router.post("/addevents", isLoggedIn, isVerified, events.addEvents);
router.post("/allevents", isLoggedIn, isVerified, events.allEvents);

router.get("/teams", isLoggedIn, accessCheck, events.participants);

// bank account details
router.post("/api/bankdetails", isLoggedIn, isVerified, bankDetails.addBankDetails);

// Paytm routes
router.post("/api/paynow", register.hasAuth, paytm.makePayment);
router.post("/api/callback", paytm.callback);

//app routes
router.post("/app/status", app.status);
router.post("/app/signup", app.register);

router.post("/app/createteam", app.createTeam);
router.post("/app/jointeam", app.joinTeam);

router.post("/app/leaveteam", app.leaveTeam);
router.post("/app/removeuser", app.removeUser);

router.post("/app/registeredevents", app.registeredEvents);
router.post("/app/teamdetails", app.teamDetails);
router.post("/app/updatedrive", app.updateDrive);
router.post("/app/states", app.states);

router.get("/app/teamlist", events.getTeams);
//router.post('/lower', events.lowercase)

//app redirect routes
router.get("/app/android", (req, res) => {
  res
    .status(301)
    .redirect(
      "https://play.google.com/store/apps/details?id=com.mit.techtatva_2020",
    );
});

router.get("/app/iOS", (req, res) => {
  res
    .status(301)
    .redirect("https://apps.apple.com/us/app/techtatva/id1538314787");
});

module.exports = router;
