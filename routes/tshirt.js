const axios = require('axios');
const { check, validationResult } = require('express-validator');
const Tshirt = require('../models/Tshirt');

const exp = {};

const verifyCaptcha = (req, res, next) => {

    if(
        req.body.v1 === undefined ||
        req.body.v1 === '' ||
        req.body.v1 === null
    ){
        return res.send({success: false,msg: 'Please Select Captcha'});
    }

    const secretKey = process.env.CAPTCHA_SECRET_KEY;

    const verifyUrl = `https://www.google.com/recaptcha/api/siteverify?secret=${secretKey}&response=${req.body.v1}&remoteip=${req.connection.remoteAddress}`

    axios.post(verifyUrl)
    .then((response) => {
        if(response.data.success !== undefined && !response.data.success){
            return res.send({success: false,msg: 'Please try again! Captcha failed'});
        }
        console.log('sdadasd')
        next();
    })
    .catch((err) => {
        console.log(err.toString());
        return res.send({success: false,msg: 'Please try again! Captcha failed'});
    })

  };


exp.buyTshirt = ([
    check("name", "Enter a valid Name").exists(),
    check("email", "Enter a valid Email").isEmail(),
    check("address", "Enter a valid Address").exists(),
    check("transactionID", "Enter your Transaction ID").exists(),
    check("phoneNo", "Enter your Phone Number").exists(),
    check("v1", "Solve the captcha again").exists(),
  ],async(req,res) =>{
      try{
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
          return res.status(400).json({ errors: errors.array() });
        }

        let {name,email,phoneNo,address,transactionID} = req.body;

        email = email.toLowerCase();
        const tshirt = new Tshirt({
            name,
            email,
            phoneNo,
            address,
            transactionID
        });

        await tshirt.save();

        return res.send({success:true,msg:"Successfully Registered. Confirmation mail will be sent after Payment Confirmation."})
      }
      catch(err){
        console.log(err);
        return res.send({ success: false, msg: "Internal Server Error" });
      }
})

module.exports = exp;