const express = require('express')
const https = require("https");
const qs = require("querystring");
const parseJson = express.json({ extended: false });
const parseUrl = express.urlencoded({ extended: false });
const PaytmChecksum = require("../paytm/checksum");
const PaytmConfig = require("../paytm/config");
const http = require("http");
const path = require("path");
const fs = require("fs");
//const DelCard = require("../models/DelCards");
//const User = require("../models/Users");
const { body } = require('express-validator');
//const Current = require('../models/Current');
let exp = {};

exp.makePayment = ([parseUrl, parseJson] , async (req, res) => { 
  let body = "";
  // => this is to be used
  const data = JSON.parse(JSON.stringify(req.body));
  const del_card = await DelCard.findOne({
    type: data.type
  });

  let price = 0;
  if(req.session.isMAHE){
    price = del_card.MAHE_price;
  }
  else
    price = del_card.Non_price;

  const orderId = "TT_" + new Date().getTime();

      const paytmParams = {};

      paytmParams.body = {
        requestType: "Payment",
        mid: PaytmConfig.PaytmConfig.mid,
        websiteName: PaytmConfig.PaytmConfig.website,
        orderId: orderId,
        callbackUrl: "http://localhost:5000/api/callback",
        txnAmount: {
          value: price,
          currency: "INR",
        },
        userInfo: {
          custId: req.session.email,
        },
      };
      console.log(paytmParams)
      //mongo query to insert orderid into User
        await User.findOneAndUpdate(
          { "email": req.session.email },
          { "$push" : {
              "delcards": {
                "type": data.type,
                "txn_id": orderId
              }
          }}
        )
          let curr_user = new Current({
            "email": req.session.email,
            "orderId": orderId
          })
          await curr_user.save()
      PaytmChecksum.generateSignature(
        JSON.stringify(paytmParams.body),
        PaytmConfig.PaytmConfig.key
      ).then(function (checksum) {
        paytmParams.head = {
          signature: checksum,
        };

        var post_data = JSON.stringify(paytmParams);

        var options = {
          /* for Staging */
          hostname: "securegw-stage.paytm.in",

          /* for Production */
          // hostname: 'securegw.paytm.in',

          port: 443,
          path: `/theia/api/v1/initiateTransaction?mid=${PaytmConfig.PaytmConfig.mid}&orderId=${orderId}`,
          method: "POST",
          headers: {
            "Content-Type": "application/json",
            "Content-Length": post_data.length,
          },
        };
        console.log('ff')
        var response = "";
        var post_req = https.request(options, function (post_res) {
          post_res.on("data", function (chunk) {
            response += chunk;
          });

          post_res.on("end", function () {
            response = JSON.parse(response);
            console.log("txnToken:", response);

            res.writeHead(200, { "Content-Type": "text/html" });
            res.write(`<html>
                                <head>
                                    <title>Show Payment Page</title>
                                </head>
                                <body>
                                    <center>
                                        <h1>Please do not refresh this page...</h1>
                                    </center>
                                    <form method="post" action="https://securegw-stage.paytm.in/theia/api/v1/showPaymentPage?mid=${PaytmConfig.PaytmConfig.mid}&orderId=${orderId}" name="paytm">
                                        <table border="1">
                                            <tbody>
                                                <input type="hidden" name="mid" value="${PaytmConfig.PaytmConfig.mid}">
                                                    <input type="hidden" name="orderId" value="${orderId}">
                                                    <input type="hidden" name="txnToken" value="${response.body.txnToken}">
                                         </tbody>
                                      </table>
                                                    <script type="text/javascript"> document.paytm.submit(); </script>
                                   </form>
                                </body>
                             </html>`);
            res.end();
          });
        });
        console.log('hmm')
        console.log(post_data)
        post_req.write(post_data);
        post_req.end();
      });

})

  exp.callback = async (req, res) => {
    try{        
                const data = JSON.parse(JSON.stringify(req.body))

                const paytmChecksum = data.CHECKSUMHASH

                var isVerifySignature = PaytmChecksum.verifySignature(data, PaytmConfig.PaytmConfig.key, paytmChecksum)
                if (isVerifySignature) {
                    console.log("Checksum Matched");

                    var paytmParams = {};

                    paytmParams.body = {
                        "mid": PaytmConfig.PaytmConfig.mid,
                        "orderId": data.ORDERID,
                    };

                    PaytmChecksum.generateSignature(JSON.stringify(paytmParams.body), PaytmConfig.PaytmConfig.key).then(async function (checksum) {
                        paytmParams.head = {
                            "signature": checksum
                        };

                        var post_data = JSON.stringify(paytmParams);

                        var options = {

                            /* for Staging */
                            hostname: 'securegw-stage.paytm.in',

                            /* for Production */
                            // hostname: 'securegw.paytm.in',

                            port: 443,
                            path: '/v3/order/status',
                            method: 'POST',
                            headers: {
                                'Content-Type': 'application/json',
                                'Content-Length': post_data.length
                            }
                        };

                        // Set up the request
                        var response = "";
                        var post_req = https.request(options, function (post_res) {
                            post_res.on('data', function (chunk) {
                                response += chunk;
                            });

                            post_res.on('end', async function () {
                                console.log('Response: ', response);
                                let resp = JSON.parse(response)
                                if (
                                  resp.body.resultInfo.resultStatus ==
                                  "TXN_SUCCESS"
                                ) {
                                  let curr_user = await Current.findOne({
                                    orderId: resp.body.orderId
                                  })
                                  let email1 = curr_user.email
                                  await User.updateOne(
                                    {"email": email1,
                                    "delcards.txn_id": resp.body.orderId
                                  },
                                    {
                                      "$set": {
                                        "delcards.$.success": true
                                      }
                                    }
                                  )
                                  
                                  await User.findOneAndUpdate(
                                    {
                                      "email": email1
                                    },
                                    {
                                      '$push': {
                                        "payments" : {
                                            "paid": true,
                                            "amount": resp.body.txnAmount,
                                            "order_id": resp.body.orderId,
                                            "timestamp": resp.body.txnDate
                                        }
                                      }
                                    }
                                  )
                                  //res.redirect(`/api/txn_success/orderId=${orderId}etc...`);
                                  // api/txn_success/...
                                } else {
                                  // failed Transaction not recorded
                                }
                                  res.write(response);
                                  
                                res.end()
                            });
                        });

                        // post the data
                        
                        post_req.write(post_data);
                        post_req.end();
                    });
                } else {
                    console.log("Checksum Mismatched");
                }

    }
    catch(err){
      console.log(err)
    }
  };
  
  module.exports = exp;