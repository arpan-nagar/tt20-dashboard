const Event = require("../models/Event");
const User = require("../models/User");
const Team = require("../models/Team");
const fetch = require("node-fetch");
const randomstring = require("randomstring");
const { Parser } = require("json2csv");
const axios = require("axios");
const exp = {};

exp.fullevents = async (req, res) => {
  try {
    const events = await Event.find({});
    const category = await Event.distinct("category");
    const tags = [
      "gaming",
      "quiz",
      "adventure",
      "debate",
      "design",
      "others",
      "business",
      "biotech",
      "puzzles",
      "coding",
      "development",
      "electrical",
      "civil",
      "creativity",
      "robotics",
      "cybersec",
      "electronics",
    ].sort();
    const userID = req.session.userID;
    const exists = await User.findOne({ userID });
    const verified = exists.verified;
    return res.render("events.ejs", {
      events,
      tags,
      category,
      userID,
      verified,
    });
  } catch (err) {
    console.log(err);
  }
};

//Test functions to send data
exp.results = async (req, res) => {
  try {
    const events = await Event.find({});
    const category = await Event.distinct("category");
    const tags = [
      "gaming",
      "quiz",
      "adventure",
      "debate",
      "design",
      "others",
      "business",
      "biotech",
      "puzzles",
      "coding",
      "development",
      "electrical",
      "civil",
      "creativity",
      "robotics",
      "cybersec",
      "electronics",
    ].sort();
    const userID = req.session.userID;
    const exists = await User.findOne({ userID });
    const verified = exists.verified;
    return res.render("results.ejs", {
      events,
      tags,
      category,
      userID,
      verified,
    });
  } catch (err) {
    console.log(err);
  }
};

exp.singleResult = async (req, res) => {
  const eventID = req.params.id;
  const event = await Event.findOne({ eventID });
  // console.log(event)
  const results = await axios.get(
    `https://categories.techtatva.in/resultsall/${eventID}`,
    {
      headers: {
        "Content-Type": "application/json",
      },
      withCredentials: true,
    },
  );
    console.log('hello',results.data)
    let result = results.data
  userID = req.session.userID;
  const exists = await User.findOne({ userID });
  const verified = exists.verified;
  return res.render("single_result.ejs", {
    event,
    result,
    userID,
    verified,
    type: event.eventType,
  });
};

exp.filter = async (req, res) => {
  try {
    const { category, tag } = req.query;

    let events;

    if (category === "All" && tag === "All") {
      events = await Event.find({});
    } else if (category === "All" && tag !== "All") {
      events = await Event.find({ tags: tag });
    } else if (category !== "All" && tag === "All") {
      events = await Event.find({ category });
    } else {
      events = await Event.find({ category, tags: tag });
    }
    // console.log(events);

    const categories = await Event.distinct("category");
    const tags = [
      "gaming",
      "quiz",
      "adventure",
      "debate",
      "design",
      "others",
      "business",
      "biotech",
      "puzzles",
      "coding",
      "development",
      "electrical",
      "civil",
      "creativity",
      "robotics",
      "cybersec",
      "electronics",
    ].sort();
    const userID = req.session.userID;
    const exists = await User.findOne({ userID });
    const verified = exists.verified;
    return res.render("events.ejs", {
      events,
      tags,
      category: categories,
      userID,
      verified,
    });
  } catch (err) {
    console.log(err);
  }
};

exp.registered = async (req, res) => {
  let all = []; //{101, }
  const user = await User.findOne({ userID: req.session.userID });
  // team
  let verified = user.verified;
  //event: [174,174,174]
  let regunique = [...new Set(user.regEvents)];

  for (let i = 0; i < regunique.length; i = i + 1) {
    let id = regunique[i];

    const curr_event = await Event.findOne({ eventID: id });

    const team = await Team.find({
      eventName: curr_event.name,
      members: req.session.userID,
    });
    for (let j = 0; j < team.length; j++) {
      let data = {
        ...curr_event._doc,
        teamID: team[j].teamID,
      };

      all.push(data);
    }
  }
  //console.log(all)
  const userID = req.session.userID;
  return res.render("registered.ejs", { all, userID, verified });
};

exp.singleEvent = async (req, res) => {
  const eventID = req.params.id;
  const event = await Event.findOne({ eventID });
  // console.log(event)
  userID = req.session.userID;
  const exists = await User.findOne({ userID });
  const verified = exists.verified;
  return res.render("single_event.ejs", {
    event,
    userID,
    verified,
    type: event.eventType,
  });
};

exp.regEvent = async (req, res) => {
  const eventID = req.params.eventID;
  const teamID = req.params.teamID;
  const event = await Event.findOne({ eventID });
  if (!event) {
    return res.redirect("/events");
  }
  userID = req.session.userID;

  const teamok = await User.findOne({
    userID: req.session.userID,
    teamList: Number(teamID),
  });

  if (!teamok) {
    return res.redirect("/events");
  }
  const teamDetails = await Team.findOne({ teamID });
  let userDetails = [];
  for (let x of teamDetails.members) {
    const user = await User.findOne({ userID: x }, { name: 1, _id: 0 });
    if (user) {
      userDetails.push(user.name);
    }
  }
  const result = await axios.get(
    `https://categories.techtatva.in/getLink/${eventID}`,
    {
      headers: {
        "Content-Type": "application/json",
      },
      withCredentials: true,
    },
  );

  console.log(result.data);
  let link = result.data;

  const exists = await User.findOne({ userID });
  const verified = exists.verified;
  return res.render("regevents.ejs", {
    event,
    userID,
    teamDetails,
    verified,
    userDetails,
    link,
  });
};

exp.participants = async (req, res) => {
  try {
    const eventID = req.query.eventID;

    const event = await Event.findOne({ eventID });
    if (event.maxMembers == 1) {
      const users = await User.find(
        { regEvents: eventID },
        { userID: 1, name: 1, _id: 0 },
      );
      return res.send({ users });
    }
    const teams = await Team.find(
      { eventName: event.name },
      { leader: 1, members: 1, _id: 0 },
    );
    return res.send({ teams });
  } catch (err) {
    console.log(err);
  }
};
//1 player
exp.registerEvent = async (req, res) => {
  try {
    const { userID, eventID, category } = req.body;
    console.log(req.body);

    const curr_event = await Event.findOne({ eventID, category });
    if (!curr_event) {
      return res.send({ success: false, msg: "Event does not exist." });
    }
    const deadline = new Date(curr_event.deadline);
    const currentTime = new Date();
    console.log(deadline, currentTime);
    if (deadline - currentTime < 0) {
      return res.send({ msg: "Event registration is now closed" });
    }
    // return res.send({success: false, msg: "Event registration will start soon!"})
    const user = await User.findOne({ userID });

    const exists = user.regEvents.includes(curr_event.eventID);

    if (exists) {
      return res.send({
        success: false,
        msg: "Already Registered for the Event.",
      });
    }

    let id_obj = await Team.find({}, { teamID: 1, _id: 0 })
      .sort({ teamID: -1 })
      .limit(1);
    let teamID = 1000;
    if (id_obj[0]) {
      teamID = id_obj[0].teamID + 1;
    }

    let party_code = randomstring.generate({
      length: 6,
      charset: "alphanumeric",
    });
    const codes = await Team.findOne({}, { partyCode: 1, _id: 0 });

    const team = new Team({
      teamID: teamID,
      eventName: curr_event.name,
      minMembers: 1,
      partyCode: party_code,
      maxMembers: 1,
      leader: userID,
      members: [userID],
    });
    await team.save();

    await User.updateOne({ userID }, { $push: { regEvents: eventID } });
    await User.updateOne({ userID }, { $push: { teamList: teamID } });

    return res.send({
      success: true,
      msg:
        "Successfully Registered for the Event. Visit registered tab to view team details.",
    });
  } catch (err) {
    console.log(err);
    return res.send({ success: false, msg: "Internal Server Error" });
  }
};

exp.createTeam = async (req, res) => {
  try {
    const { userID, eventID, category } = req.body;
    const curr_event = await Event.findOne({ eventID, category });
    const deadline = new Date(curr_event.deadline);
    const currentTime = new Date();

    const user = await User.findOne({ userID });
    const exists = user.regEvents.includes(eventID);

    if (exists) {
      return res.send({
        success: false,
        msg:
          "Already registered for the event. Visit registered tab to view team details.",
      });
    }

    if (deadline - currentTime < 0) {
      return res.send({
        success: false,
        msg: "Event registration is now closed",
      });
    }

    let id_obj = await Team.find({}, { teamID: 1, _id: 0 })
      .sort({ teamID: -1 })
      .limit(1);
    let teamID = 1000;
    if (id_obj[0]) {
      teamID = id_obj[0].teamID + 1;
    }

    let party_code = randomstring.generate({
      length: 6,
      charset: "alphanumeric",
    });
    const codes = await Team.findOne({}, { partyCode: 1, _id: 0 });

    const team = new Team({
      teamID: teamID,
      eventName: curr_event.name,
      minMembers: curr_event.minMembers,
      partyCode: party_code,
      maxMembers: curr_event.maxMembers,
      leader: userID,
      members: [userID],
    });
    await team.save();

    await User.updateOne({ userID }, { $push: { regEvents: eventID } });
    await User.updateOne({ userID }, { $push: { teamList: teamID } });

    res.send({ success: true, msg: "Successfully created a team. Visit registered tab to view team details" });
  } catch (err) {
    console.log(err);
    return res.send({ success: false, msg: "Internal Server Error" });
  }
};

exp.joinTeam = async (req, res) => {
  try {
    const { userID, eventID, category, partyCode } = req.body;
    const curr_event = await Event.findOne({ eventID, category });
    const deadline = new Date(curr_event.deadline);
    const currentTime = new Date();
    const user = await User.findOne({ userID });
    const team = await Team.findOne({ partyCode });

    if (!user || !team) {
      return res.send({ success: false, msg: "Invalid Party Code." });
    }

    const exists = team.members.includes(userID);
    if (exists) {
      return res.send({ msg: "You are already part of the team." });
    }

    // if(deadline - currentTime < 0){
    //   return res.send({success: false, msg: 'Event registration is now closed'})
    // }
    //const maxMembers = team.maxMembers;
    const maxMembers = curr_event.maxMembers;
    if (team.members.length === maxMembers) {
      return res.send({ msg: "Team is already full." });
    }
    const added = await Team.updateOne(
      { partyCode },
      { $push: { members: userID } },
    );
    //check if event already pushed
    await User.updateOne({ userID }, { $push: { regEvents: eventID } });
    await User.updateOne({ userID }, { $push: { teamList: team.teamID } });

    res.send({ success: true, msg: "Successfully joined the team. Visit registered tab to view team details" });
  } catch (err) {
    console.log(err);
    return res.send({ success: false, msg: "Internal Server Error" });
  }
};

exp.removeUser = async (req, res) => {
  try {
    const { userID, eventID, teamID, removeID } = req.body;
    const team = await Team.findOne({ teamID });
    if (team.leader != userID) {
      return res.send({ msg: "Only leader can remove team members" });
    }
    if (userID == removeID) {
      return res.send({
        success: false,
        msg: "Leader cannot be removed from the team.",
      });
    }

    await Team.updateOne({ teamID }, { $pull: { members: removeID } });

    await User.updateOne(
      { userID: removeID },
      { $pull: { regEvents: eventID } },
    );
    await User.updateOne({ userID: removeID }, { $pull: { teamList: teamID } });
    return res.send({
      success: true,
      msg: "Successfully removed member from the team.",
    });
  } catch (err) {
    console.log(err);
    return res.send({ success: false, msg: "Internal Server Error" });
  }
};

exp.leaveTeam = async (req, res) => {
  try {
    const { userID, eventID, teamID } = req.body;
    const team = await Team.findOne({ teamID });


    if (team.leader == userID && team.members.length > 1) {
      return res.send({ success: false, msg: "Please remove other members before leaving the team." });
    }

    await Team.updateOne({ teamID }, { $pull: { members: userID } });

    await User.updateOne({ userID }, { $pull: { regEvents: eventID } });
    await User.updateOne({ userID }, { $pull: { teamList: teamID } });

    return res.send({
      success: true,
      msg: `Successfully left the team ${teamID}`,
    });
  } catch (err) {
    console.log(err);
    return res.send({ success: false, msg: "Internal Server Error" });
  }
};

//=========================

exp.addEvents = async (req, res) => {
  try {
    console.log("hello");
    //let url  = 'https://categories.techtatva.in/app/events'
    let url = "http://localhost:6900/app/rawEvents";
    const data = await fetch(url, {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
    });
    const events = await data.json();
    //console.log(events.data)
    let count = 100;
    allEvents = [];
    for (var x of events.data) {
      var min, max;
      if (x.teamSize == undefined || x.teamSize == null) {
        min = max = 0;
      } else if (x.teamSize.length > 3) {
        min = max = 0;
      } else if (x.teamSize.length == 3) {
        min = Number(x.teamSize[0]);
        max = Number(x.teamSize[2]);
      } else {
        min = max = Number(x.teamSize);
      }
      let eventHeads = [];
      for (let y of x.eventHead) {
        //console.log(y.name, y.phoneNo)
        eventHeads.push({
          name: y.name,
          phoneNo: y.phoneNo != undefined ? y.phoneNo : 0,
        });
      }
      console.log(x.name, x.mode);
      //console.log(x.name, x.teamSize, min, max)
      let event = await Event({
        eventID: x.eventID,
        name: x.name,
        category: x.category,
        description: x.description,
        eventType: x.eventType,
        mode: x.mode,
        participationCriteria: "",
        minMembers: min,
        maxMembers: max,
        prize: x.prize != null ? x.prize : 0,
        eventHead: eventHeads,
        tags: x.tags,
      });
      if (event) {
        allEvents.push(event);
      }
    }
    const result = await Event.insertMany(allEvents);
    return res.send({ msg: "Events added" });
  } catch (err) {
    console.log(err);
  }
};

exp.allEvents = async (req, res) => {
  try {
    const events = await Event.find({});
    console.log(events);
    return res.send(events);
  } catch (err) {
    console.log(err);
  }
};

exp.getTeams = async (req, res) => {
  try {
    const { event, apikey } = req.query;

    if (apikey != "db1db9e8-87a5-454a-b01d-fda63a2d6d8f") {
      return res.send({ success: false, msg: "Not Authenticated" });
    }

    let fields = ["name", "userID", "teamID", "email", "phone", "status"];
    const json2csv = new Parser({ fields });
    const participants = await Team.aggregate([
      { $match: { eventName: event } },
      { $unwind: "$members" },
      { $project: { members: 1, teamID: 1 } },
    ]);
    console.log(participants);
    for (let i = 0; i < participants.length; i++) {
      const user = await User.findOne(
        { userID: participants[i].members },
        { name: 1, email: 1, phoneNo: 1, verified: 1 },
      );
      console.log(user, participants[i].member);
      if (user) {
        participants[i] = {
          name: user.name,
          userID: participants[i].members,
          teamID: participants[i].teamID,
          email: user.email,
          phone: user.phoneNo,
          status: user.verified,
        };
      }
    }

    console.log(participants);
    const csv = json2csv.parse(participants);
    res.header("Content-Type", "text/csv");
    res.attachment(`${event}.csv`);
    return res.send(csv);
  } catch (err) {
    console.log(err);
    return res.send({ success: false, msg: "Internal Server Error" });
  }
};

exp.updateDrive = async (req, res) => {
  try {
    const { driveLink } = req.body;

    await User.updateOne(
      { userID: req.session.userID },
      { $set: { driveLink } },
    );
    return res.send({ success: true, msg: "Drive Link successfully updated" });
  } catch (err) {
    console.log(err);
    return res.send({ success: false, msg: "Internal Server Error" });
  }
};

// exp.addTime = async(req,res) => {
//   try {
//     const events = await Event.find({})
//     //console.log(events)
//     const date = 'November 8, 2020 03:24:00'
//     const result = await Event.updateMany({}, {$set : {deadline : date}})
//     // for(let x of events)
//     // {
//     //   console.log(x.name)
//     //   const result = await Event.upda
//     // }
//     return res.send('updated')
//   } catch (err) {
//     console.log(err)
//   }
// }
//{ email : { $regex: /[A-Z]/ } }

// exp.lowercase = async(req,res) => {
//   try {
//     const ex = await User.find({
//       email : { $regex: /[A-Z]/ }
//     });
//     console.log(ex);
//     for(let i of ex){
//       console.log(i.email)
//       let email1 = (i.email).toLowerCase();
//       const ex = await User.updateOne({
//         email: i.email
//       },
//       {"$set": {
//         email: email1
//       }})

//       return res.send({
//         success: true
//       })
//     }
//   } catch (err) {
//     console.log(err)
//   }
// }
module.exports = exp;
