const aws = require('aws-sdk');
let exp = {};
const mailer = require('nodemailer');

const config = new aws.Config({
  accessKeyId: process.env.ACCESS_KEY_ID,
  secretAccessKey: process.env.SECRET_ACCESS_KEY,
  region: process.env.AWS_REGION,
});

aws.config = config;

const transporter = mailer.createTransport({
  SES: new aws.SES(),
});

exp.sendPassLink = async (obj) => {
  try {
    const to = obj.email;
    const from = 'Techtatva 2020 <sysadm@mitportals.in>';
    const link = `${process.env.DOMAIN}changepass/${obj.token}`;
    const html = `
            <html>

            <body> 
            Hello,<br>

            Here is your reset link: <a href="${link}">${link}</a>. <br>
            <br>
            <br>
            Thank you,<br>
            System Admin and Web Development, TechTatva'20.
            </body>

            </html>

        `;
    transporter.sendMail({
      from,
      to,
      html,
      subject: "Password reset for TechTatva'20.",
    });

    console.log('Mail sent.');
  } catch (err) {
    console.log(err);
  }
};

module.exports = exp;
