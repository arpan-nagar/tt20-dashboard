const mongoose = require('mongoose');

const CurrSchema = new mongoose.Schema({
    email: String,
    orderId: {
        type: String,
        required: true
    }
})

module.exports = Current = mongoose.model('Current', CurrSchema);