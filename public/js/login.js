var currentTab = 0; // Current tab is set to be the first tab (0)
showTab(currentTab); // Display the current tab

function showTab(n) {
  // This function will display the specified tab of the form ...
  var x = document.getElementsByClassName("tab");
  x[n].style.display = "block";
  // ... and fix the Previous/Next buttons: #reg-button
  if (n == 0) {
    document.getElementById("prevBtn").style.display = "none";
    document.getElementById("reg-button").style.display = "none";
     document.getElementById("register-captcha").style.display = "none";
    document.getElementById("nextBtn").style.display = "inline";
    document.getElementById("helloMid").innerHTML = `Hello` + `<br>` + `There!`;
  } else {
    document.getElementById("prevBtn").style.display = "inline";
    document.getElementById("reg-button").style.display = "inline";
    document.getElementById("register-captcha").style.display = "inline";
  }
  if (n == (x.length - 1)) {
    document.getElementById("prevBtn").style.display = "inline";
    document.getElementById("nextBtn").innerHTML = "Submit";
   document.getElementById("nextBtn").style.display = "none";
   document.getElementById("reg-button").style.display = "inline";
   document.getElementById("register-captcha").style.display = "inline";
    document.getElementById("helloMid").innerHTML = `Almost` + `<br>` + `Done!`;
    document.getElementById("titlec").style.marginBottom = "-4.5em";
  } else {
      if(n != 0){
        document.getElementById("nextBtn").innerHTML = "Next";
        document.getElementById("prevBtn").style.display = "inline";
        document.getElementById("reg-button").style.display = "none";
        document.getElementById("register-captcha").style.display = "none";
      }
  }
  if( n == (x.length - 2)) {
    document.getElementById("nextBtn").style.display = "inline";
  }
  // ... and run a function that displays the correct step indicator:
  fixStepIndicator(n)
}

function nextPrev(n) {
  // This function will figure out which tab to display
  var x = document.getElementsByClassName("tab");
  // Exit the function if any field in the current tab is invalid:
  if (n == 1 && !validateForm()) return false;
  // Hide the current tab:
  x[currentTab].style.display = "none"; 
  // Increase or decrease the current tab by 1:
  currentTab = currentTab + n;
  // if you have reached the end of the form... :
  if (currentTab >= x.length) {
    //...the form gets submitted:
    document.getElementById("regForm").submit();
    return false;
  }
  // Otherwise, display the correct tab:
  showTab(currentTab);
}

function validateForm() {
    // This function deals with validation of the form fields
    var x, y, i, valid = true;
    // x = document.getElementsByClassName("tab");
    // y = x[currentTab].getElementsByTagName("input");
    // // A loop that checks every input field in the current tab:
    // for (i = 0; i < y.length; i++) {
    //   // If a field is empty...
    //   if (y[i].value == "") {
    //     // add an "invalid" class to the field:
    //     y[i].className += " invalid";
    //     // and set the current valid status to false:
    //     valid = false;
    //   }
    // }
    // // If the valid status is true, mark the step as finished and valid:
    // if (valid) {
    //   document.getElementsByClassName("step")[currentTab].className += " finish";
    // }
    return valid; // return the valid status
}
  
function fixStepIndicator(n) {
    // This function removes the "active" class of all steps...
    var i, x = document.getElementsByClassName("step"); 
    for (i = 0; i < x.length; i++) {
      x[i].className = x[i].className.replace(" active", "");
    }
    //... and adds the "active" class to the current step:
    // x[x.length].className += " active";
}
var a, b;
var onloadCallback = function() {
     a = grecaptcha.render('login-captcha', {
      'sitekey' : '6Lf7g9wZAAAAAE2dN7HreAvpYa5S1Iqm627DKf65',
      'theme': 'light'
    });
     b = grecaptcha.render('register-captcha', {
        'sitekey' : '6Lf7g9wZAAAAAE2dN7HreAvpYa5S1Iqm627DKf65',
        'theme': 'light'
      });
  };
window.addEventListener('load', () => {
    document.querySelector('#submit-btn-login').addEventListener('click', (e) => {
        document.querySelector('#submit-btn-login').innerHTML = 'Loading....';
        e.preventDefault();
        let email = document.querySelector('#email-login').value 
        let password = document.querySelector('#password-login').value
        let captcha = document.querySelector('#login-captcha').value
        var v = grecaptcha.getResponse(a);
        console.log(v)
        var validate = () => {
            function validateEmail(email) {
                const re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                return re.test(email);
            }
            if(password == ""){
                alert('Please Enter your Password');
                return false;
            }
            if(!validateEmail(email)){
                alert('Please Enter your Email');
                return false;
            }
            return true;
        }
        if(validate()){
            let data = {
                "email": email,
                "password": password,
                "captcha": v
            }
            console.log(data)
            fetch("/api/login", {
                method: "POST",
                headers: {
                    "Content-Type": "application/json"
                },
                credentials: "include",
                body: JSON.stringify(data),
                redirect: "error"
            })
                .then((res) => {
                    console.log(res);
                    return res.json()
                })
                .then((json) => {
                    if (json.success) {
                            window.location.replace("/events");
                    }
                    else {
                        alert(json.msg);
                        document.querySelector('#submit-btn-login').innerHTML = 'Submit';
                    }
                })  
        }
        else{
            document.querySelector('#submit-btn-login').innerHTML = 'Submit';
        }
    })
    var sb = document.getElementById('reg-button');
    sb.addEventListener('click', (e) => {
        sb.innerHTML = 'Loading.....'
        let name = document.querySelector('#name').value 
        let phonenumber = document.querySelector('#phone').value
        let email = document.querySelector('#email').value 
        let password = document.querySelector('#password-reg').value
        let branch = document.querySelector('#branch').value 
        let link = document.querySelector('#drive-link').value
        let p2 = document.querySelector('#confirmpassword-reg').value
        var e = document.getElementById("state");
        var state = e.options[e.selectedIndex].value;
        let college = document.querySelector('#college').value
        let captcha = document.querySelector('#register-captcha').value
        let v1 = grecaptcha.getResponse(b);
        const validate = () => {
            function validateEmail(email) {
                const re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                return re.test(email);
            }
            function url(u){
                var expression = /[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\+.~#?&//=]*)?/gi;
                return expression.test(u);
            }
            function isNumeric(str) {
                if (typeof str != "string") return false // we only process strings!  
                return !isNaN(str) && // use type coercion to parse the _entirety_ of the string (`parseFloat` alone does not do this)...
                       !isNaN(parseFloat(str)) // ...and ensure strings of whitespace fail
              }
            if(password == ""){
                alert('Please Enter your Password');
                return false;
            }
            if(password != p2){
                alert('Password does not match');
                return false;
            }
            if(state == ""){
                alert('Please Enter your State');
                return false;
            }
            if(college == ""){
                alert('Please Enter your College');
                return false;
            }
            if(name == ""){
                alert('Please Enter your Name');
                return false;
            }
            if(phonenumber == ""){
                alert('Please Enter your 10 digit Phone Number');
                return false;
            }
            if(branch == ""){
                alert('Please Enter your branch');
                return false;
            }
            if(link == ""){
                alert('Please Enter your Google Drive Link');
                return false;
            }
            if(!validateEmail(email)){
                alert('Please Enter your Email');
                return false;
            }
            if(!url(link)){
                alert('Please Enter a proper Google Drive URL.');
                return false;
            }
            return true;
        }
        if(validate()){ 
            data = {
                "name": name,
                "email": email,
                "phoneNo": phonenumber,
                "driveLink": link,
                "branch": branch,
                "password": password,
                "state": state,
                "college": college,
                "isMahe": true,
                "captcha": v1
            }
            console.log(data)
            fetch("/api/signup", {
                method: "POST",
                headers: {
                    "Content-Type": "application/json"
                },
                credentials: "include",
                body: JSON.stringify(data),
                redirect: "error"
            })
                .then((res) => {
                    console.log(res);
                    return res.json()
                })
                .then((json) => {
                    if (json.success) {
                            window.location.replace("/events");
                    }
                    else {
                        if(json.msg == 'Please try again! Captcha failed')
                        alert(json.msg + ". Please try refreshing the page and re-entering details.");
                        else
                        alert(json.msg)
                        sb.innerHTML = 'Register'
                    }
                })
        } else sb.innerHTML = 'Register'
    })
})