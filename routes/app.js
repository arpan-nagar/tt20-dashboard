const bcrypt = require('bcryptjs');
const randomstring = require('randomstring')
const { check, validationResult } = require('express-validator');
const Event = require('../models/Event');
const User = require('../models/User');
const Team = require('../models/Team')

const exp = {}
//     auth middleware
const verifyCredentials = async(req,res,next) =>{
    const {email,password,key} = req.body;

    if(key !== process.env.APPKEY){
        return res.send({success:false,msg:"Not Authenticated"});
    }

    if(!email || !password){
        return res.send({success:false,msg:"Please enter Valid Credentials"});
    }

    const exist = await User.findOne({ email });
    if (!exist) {
      return res.send({ success: false, msg: 'Invalid Credentials' });
    }

    const ok = await bcrypt.compare(password, exist.password);
    if (!ok) {
      return res.send({ success: false, msg: 'Invalid Credentials' });
    }
    next();
}

const isVerified = async(req,res,next) =>{
    const {userID,email} = req.body;
    const user = await User.findOne({userID,email});

    if(!user){
        return res.send({success:false,msg:"User not found."});
    }

    if(user.verified == 'UNVERIFIED'){
        return res.send({success:false,msg:"You have not been verified yet."})
    }

    if(user.verified == 'REJECTED'){
        return res.send({success:false,msg:"User registration rejected. Contact System Admin."})
    }
    next();
}

exp.status = ([verifyCredentials,async (req,res)=>{
    try{
        const {email} = req.body;
        let user = await User.findOne({email},{_id:0,role:0,token:0,password:0});
    
        const team_details = [];
    
        for(let i=0;i<user.regEvents.length;i++){
            const event = await Event.findOne({eventID:user.regEvents[i]});
            const team = await Team.findOne({members:user.userID,eventName:event.name});
    
            const temp = {
                eventID:event.eventID,
                teamID:team.teamID
            }
            team_details.push(temp);
        }
    
        user = user.toJSON();
        user["teamDetails"] = team_details;
    
        return res.send({success:true,data:user});
    }
    catch(err){
        console.log(err);
        return res.send({success:false,msg:"Internal Server Error"})
    }
    
}])

exp.register = ([
    check("name", "Enter a valid Name").exists(),
    check("email", "Enter a valid Email").isEmail(),
    check("password", "Password is required").exists(),
    check("college", "Enter your College name").exists(),
    check("driveLink", "Enter the Google Drive Link").exists(),
    check("phoneNo", "Enter your Phone Number").exists(),
    check("state", "Enter a valid State").exists(),
  ],async (req, res) => {
    try {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(400).json({ errors: errors.array() });
      }
    let { name,email, phoneNo, password, college,state, isMahe, driveLink } = req.body;
    const exists = await User.findOne({ email });
    if (exists) {
        return res.send({ 
        success: false,
        msg: 'Email already in use.',
        });
    }

    const drive = await User.findOne({ driveLink});
    if (drive) {
      return res.send({ 
        success: false,
        msg: 'Driver link already in use.',
      });
    }



    let id_obj = await User.find({}, { userID: 1, _id: 0 })
    .sort({ userID: -1 })
    .limit(1);
    let userID = 5000;
    if (id_obj[0]) {
        userID = id_obj[0].userID + 1;
    }
    
    const newUser = new User({
        userID,
        name,
        email,
        phoneNo,
        password,
        college,
        state,
        isMahe, 
        driveLink
    });
    const salt = await bcrypt.genSalt(10);
    newUser.password = await bcrypt.hash(password, salt);

    await newUser.save();

    return res.send({
        success: true,
        msg: 'Succesfully Registered.',
        data:newUser.toJSON()
    });

    } catch (err) {
      console.log(err);
      return res.send({success:false,msg:"Internal Server Error"});
    }
  });

exp.createTeam = [verifyCredentials,isVerified,async(req,res) => {
    try {
        const {userID, eventID, category} = req.body
        if(!userID || !eventID || !category ){
            return res.send({succes: false, msg:"Invalid parameters"})
        }
        const curr_event = await Event.findOne(
            {eventID, category}
        )
        const user = await User.findOne({userID})
       
        const exists = user.regEvents.includes(eventID);
        
        const deadline = new Date(curr_event.deadline);
        const currentTime = new Date();

        if(exists){
            return res.send({success:false,msg: "Already Registered for the Event."})
        }
        if(deadline - currentTime < 0){
            return res.send({success: false, msg: 'Event registration is now closed'})
        }

        let id_obj = await Team.find({}, { teamID: 1, _id: 0 })
        .sort({ teamID: -1 })
        .limit(1);
        let teamID = 1000;
        if (id_obj[0]) {
            teamID = id_obj[0].teamID + 1;
        }

        let party_code = randomstring.generate({
            length: 6,
            charset: 'alphanumeric'
            });
        const codes = await Team.findOne({}, {partyCode:1, _id: 0})
    
        const team = new Team({
            teamID: teamID,
            eventName: curr_event.name, 
            minMembers : curr_event.minMembers,
            partyCode : party_code,
            maxMembers : curr_event.maxMembers,
            leader: userID,
            members: [userID],
            });
        await team.save();
        
        await User.updateOne({ userID }, { $push: { regEvents: eventID } });
        await User.updateOne({ userID }, { $push: { teamList: teamID } });
        res.send({success:true,msg: "Successfully created a team.",data:team.toJSON()});
    }

     catch (err) {
        console.log(err);
        return res.send({success:false,msg:"Internal Server Error"});
    }
}]

exp.joinTeam = [verifyCredentials,isVerified,async(req,res) => {
    try {
        const {userID, eventID, category, partyCode} = req.body;
        if(!userID || !eventID || !category || !partyCode){
            return res.send({succes: false, msg:"Invalid parameters"})
        }

        const user = await User.findOne({userID})
        const team = await Team.findOne({partyCode})
        const curr_event = await Event.findOne({ eventID, category });
        if(!user || !team){
            return res.send({success:false,msg:"Invalid Party Code"});
        }

        // const eventExists = user.regEvents.includes(eventID);
        // if(eventExists){
        //     return res.send({success:false,msg: "You are already part of a team."})
        // }
        
        const exists = team.members.includes(userID);
        if(exists){
            return res.send({success:false,msg: "You are already part of a team."})
        }
        const deadline = new Date(curr_event.deadline);
        const currentTime = new Date();

        // if(deadline - currentTime < 0){
        //     return res.send({success: false, msg: 'Event registration is now closed'})
        // }

        const maxMembers = curr_event.maxMembers
        if(team.members.length === maxMembers){
            return res.send({success:false,msg: "Team is already full."})
        }
        const added = await Team.updateOne({ partyCode }, { $push: { members: userID} });

        await User.updateOne({ userID }, { $push: { regEvents: eventID } });
        await User.updateOne({ userID }, { $push: { teamList: team.teamID } });    
        return res.send({success:true,msg: "Successfully joined the team.",teamID:team.teamID})
    }

     catch (err) {
        console.log(err);
        return res.send({success:false,msg:"Internal Server Error"});
    }
}]

exp.removeUser = [verifyCredentials,isVerified,async(req,res) => {
    try {
        const {userID, eventID, teamID, removeID} = req.body
        if(!userID || !eventID || !teamID || !removeID ){
            return res.send({succes: false, msg:"Invalid parameters"})
        }
        const team  = await Team.findOne({teamID})
        if(team.leader!=userID){
            return res.send({success: false, msg : "Only leader can remove team members"})
        }
        if(userID == removeID){
            return res.send({success: false, msg: "Leader cannot be removed from the team."})
        }
        

        const updated = await Team.updateOne({teamID}, {$pull: {members : removeID}})
        if(updated.nModified){
            await User.updateOne({userID:removeID}, {$pull: {regEvents : eventID}})
            await User.updateOne({userID:removeID}, {$pull: {teamList : teamID}})
        }else{
            return res.send({success:false,msg: "User/Event doesn't exists"})
        }
        
        return res.send({success:true,msg: "Successfully removed member from the team."})
        
    } catch (err) {
        console.log(err);
        return res.send({success:false,msg:"Internal Server Error"});
    }
}]

exp.leaveTeam = [verifyCredentials,isVerified,async(req,res) => {
    try {
        const {userID, eventID, teamID} = req.body
        if(!userID || !eventID || !teamID ){
            return res.send({succes: false, msg:"Invalid parameters"})
        }
        const team  = await Team.findOne({teamID});

        if(!team){
            return res.send({succes: false, msg:"Invalid Team ID."})
        }

        if(team.minMembers == 1 && team.maxMembers == 1){
            return res.send({success: false, msg:"Cannot leave team for single events."})
        }

        if (team.leader == userID && team.members.length > 1) {
            return res.send({ success: false, msg: "Please remove other members before leaving the team." });
          }

        const updated = await Team.updateOne({teamID}, {$pull: {members : userID}})
        if(updated.nModified){   
            await User.updateOne({userID}, {$pull: {regEvents : eventID}});
            await User.updateOne({userID}, {$pull: {teamList : teamID}})
        }else{
            return res.send({success:false,msg: "User/Event doesn't exists"})
        }
        
        return res.send({success:true,msg: `Successfully left the team ${teamID}`})
        
    } catch (err) {
        console.log(err);
        return res.send({success:false,msg:"Internal Server Error"});
    }
}]

exp.states = async(req,res) => {
    try {
        const states = await User.find({}, {state:1})
        const state = {}

        for(let x of states){
            if(x.state in state){
                state[x.state]++
            }else{
                state[x.state]=1
            }
        }
        return res.send({success : true, data: state})
    } catch (err) {
        console.log(err);
        return res.send({success:false,msg:"Internal Server Error"});
    }
}

exp.registeredEvents = [verifyCredentials,async(req,res) =>{
    try{
        const {userID} = req.body;
        const user = await User.findOne({userID});

        const contact = {
            name:'Dev CC',
            phoneNo:9830032967
        }

        const eventData = [];

        for(let eventID of user.regEvents){
            const event = await Event.find({eventID},{_id:0});
            console.log(event)
            if(event[0].eventHead.length < 2){
                event[0].eventHead.push(contact);
            }
            eventData.push(event)
        }

        return res.send({success:true,data:eventData});
    }
    catch(err){
        console.log(err);
        return res.send({success:false,msg:"Internal Server Error"});
    }
}]

exp.teamDetails = [verifyCredentials,async(req,res) =>{
    try{
        const {userID,teamID} = req.body;
        const team = await Team.findOne({teamID});

        const teamData = [];

        for(let memberID of team.members){

            const member = await User.find({userID:memberID},{userID:1,name:1});
            teamData.push(member)
        }
        const result = team.toObject();
        result.members = teamData;

        return res.send({success:true,data:result});
    }
    catch(err){
        console.log(err);
        return res.send({success:false,msg:"Internal Server Error"});
    }
}]

exp.updateDrive = [verifyCredentials,async(req,res) =>{
    try{
      const {userID,driveLink} = req.body;
  
      await User.updateOne({userID},{$set:{driveLink}});
      return res.send({success:true,msg: 'Drive Link successfully updated'});
    }
    catch(err){
      console.log(err)
      return res.send({success:false,msg: 'Internal Server Error'})
    }
    
  }]



module.exports = exp;


