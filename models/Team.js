const mongoose = require('mongoose');

const TeamSchema = new mongoose.Schema({
    teamID:{
        type:Number,
        required:true,
        unique:true
    },
    timeStamp:{
        type:Date,
        default: Date.now()
    },
    eventName:{
        type:String,
        required:true
    },
    partyCode:{
        type:String,
        required:true,
        unique:true
    },
    minMembers:{
        type:Number
    },
    maxMembers:{
        type:Number
    },
    leader: { 
        type:Number
    },
    members:[
        Number
    ]
})

module.exports = Team = mongoose.model('Team', TeamSchema);