const mongoose = require('mongoose');

const BoughtCards = new mongoose.Schema({
    type: String,
    txn_id: String,
    success: {
        type: Boolean,
        default: false
    }
});

module.exports = BoughtCards1 = mongoose.model('BoughtCards', BoughtCards);