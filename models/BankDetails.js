const mongoose = require('mongoose');

const BankDetailSchema = new mongoose.Schema({
    userID:{
        type:String,
        required:true
    },
    holderName:{
        type:String,
    },
    accountNo:{
        type:String,
    },
    ifsc:{
        type:String,
    },
    bankName:{
        type:String,
    },
    branchName:{
        type:String,
    }
    
})

module.exports = BankDetail = mongoose.model('BankDetail', BankDetailSchema);