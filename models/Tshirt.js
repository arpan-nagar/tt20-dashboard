const mongoose = require('mongoose');

const TshirtSchema = new mongoose.Schema({
    name:{
        type:String,
        required:true
    },
    email:{
        type:String,
        required:true,
    },
    phoneNo:{
        type:String,
        required:true
    },
    address:{
        type:String,
        required:true
    },
    transactionID:{
        type:String,
        required:true
    }
})

module.exports = Tshirt = mongoose.model('Tshirt', TshirtSchema);