const bcrypt = require('bcryptjs');
const User = require('../models/User');
const crypto = require('crypto');
const axios = require('axios')
const { check, validationResult } = require('express-validator');
const cryptoRandomString = require('crypto-random-string');
const mailer = require('./mailer');
let exp = {};

exp.forgotPassword = ([
  check("email", "Enter a valid email").isEmail(),
],async (req, res) => {
  try {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }
    if (!req.session.logged_in) {
      const { email } = req.body;
      const exists = await User.findOne({ email });
      if (!exists) {
        return res.send({
          success: false,
          msg: 'Requested Email does not exist, please register.',
        });
      }
      let token = crypto.randomBytes(32).toString('hex');
      await User.updateOne(
        { email: email },
        {
          $set: { token: token },
        },
      );
      console.log('Token set');
      await mailer.sendPassLink({
        email: email,
        token: token,
      });

      return res.send({
        success: true,
        msg: 'Email sent, please reset password from link in email.',
      });
    }
  } catch (err) {
    console.log(err);
  }
});

exp.checkPassword = async (req, res) => {
  try {
    const { id } = req.params;
    if (id === 'invalid') {
      return res.send({
        msg: 'Redirect',
      });
    }
    const exists = await User.findOne({ token: id });
    if (!exists) {
      return res.send({
        msg: 'Invalid Token',
      });
    }
    const { password } = req.body;
    const salt = await bcrypt.genSalt(10);
    const newpassword = await bcrypt.hash(password, salt);

    await User.updateOne({ token: id }, { $set: { password: newpassword } });

    return res.send({
      success: true,
      msg: 'Password updated, please login.',
    });
  } catch (err) {
    console.log(err);
  }
};

exp.register = ([
  check("name", "Enter a valid Name").exists(),
  check("email", "Enter a valid Email").isEmail(),
  check("password", "Password is required").exists(),
  check("college", "Enter your College name").exists(),
  check("driveLink", "Enter the Google Drive Link").exists(),
  check("phoneNo", "Enter your Phone Number").exists(),
  check("state", "Enter a valid State").exists(),
],async (req, res) => {
  try {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }
    if (!req.session.logged_in || req.session.logged_in == undefined) {
      let { name,email, phoneNo, password,branch, college,state, isMahe, driveLink } = req.body;
      email = email.toLowerCase();
      const exists = await User.findOne({ email });
      if (exists) {
        return res.send({ 
          success: false,
          msg: 'Email already in use.',
        });
      }

      const drive = await User.findOne({ driveLink});
      if (drive) {
        return res.send({ 
          success: false,
          msg: 'Driver link already in use.',
        });
      }



      let id_obj = await User.find({}, { userID: 1, _id: 0 })
      .sort({ userID: -1 })
      .limit(1);
      let userID = 5000;
      if (id_obj[0]) {
          userID = id_obj[0].userID + 1;
      }
    
      const newUser = new User({
        userID,
        name,
        email,
        phoneNo,
        password,
        branch,
        college,
        state,
        isMahe, 
        driveLink
      });
      const salt = await bcrypt.genSalt(10);
      newUser.password = await bcrypt.hash(password, salt);

      const ok = await newUser.save();
      if (ok) {
        req.session.logged_in = true;
        req.session.email = email;
        req.session.isMahe = isMahe;
        req.session.userID = userID
      }
      return res.send({
        success: true,
        msg: 'Registered.',
      });
    }
    else {
      console.log("else")
      res.redirect('/')
    }
  } catch (err) {
    console.log(err);
    return res.send({success: false, msg: 'Internal Server Error.'})
  }
});

exp.status = async (req, res) => {
  if (!req.session.logged_in || req.session.logged_in == undefined) {
    return res.send({
      status: false,
    });
  }
  const user = await User.findOne({email: req.session.email});
  return res.send({
    status: true,
    data: user
  });
};

exp.login = ([
  check("email", "Enter a valid email").isEmail(),
  check("password", "Password is required").exists(),
],async (req, res) => {
  try{
  const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }
  if (!req.session.logged_in || req.session.logged_in == undefined) {
    let { email, password } = req.body;
    email = email.toLowerCase();
    const exist = await User.findOne({ email });
    if (!exist) {
      return res.send({ success: false, msg: 'Invalid Credentials' });
    }

    const ok = await bcrypt.compare(password, exist.password);
    if (!ok) {
      return res.send({ success: false, msg: 'Invalid Credentials' });
    }

    req.session.logged_in = true;
    req.session.email = email;
    req.session.isMAHE = exist.isMAHE;
    req.session.userID = exist.userID;
    return res.send({
      success: true,
      msg: 'Logged in successfully.',
    });
    
  }
  else{
    console.log('dsasdasd')
    return res.redirect('/events');
  }
}
catch(err){
  console.log(err)
  return res.send({success: false, msg: 'Internal Server Error.'})
}
});

exp.hasAuth = async(req,res,next) => {
  if(req.session.logged_in){
     next();
  }
  else
 return res.redirect('/events');
}

exp.logout = async (req, res) => {
  req.session.destroy();
  return res.redirect('/login');
};

module.exports = exp;
