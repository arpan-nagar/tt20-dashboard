const path = require("path");
require("dotenv").config();

const express = require("express");
const body = require("body-parser");
const session = require("express-session");
const redis = require("redis");
const redisStore = require("connect-redis")(session);
const morgon = require("morgan");
const cors = require("cors");
const app = express();
const connectDB = require("./config/db");
// if (process.env.MODE === "DEV") {
//   app.use(morgon("dev"));
// }
app.use(cors());
app.use(
  session({
    resave: false,
    saveUninitialized: false,
    secret: process.env.SECRET_SESSION_KEY,
    store: new redisStore({
      host: "localhost",
      port: 6379,
      client: redis.createClient(),
      ttl: 6048000
    }),
    cookie: { maxAge: 6048000 },
  })
);

// app.get('/tshirt', function(req, res) {
//     return res.sendFile('views/tshirt.html', {root: __dirname })
// });

app.use(body.json());
app.use(body.urlencoded({ extended: false }));
connectDB();

app.use("/", require("./routes/index"));
app.use(express.static(path.join(__dirname, "public")));
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "ejs");


const PORT = process.env.PORT;
app.listen(PORT, () => {
  console.log(`Server started at PORT ${PORT}`);
});
