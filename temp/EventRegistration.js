const mongoose = require("mongoose");

const EventRegister = new mongoose.Schema({
  teamID: Number,
  eventid: Number,
  round: Number,
});

module.exports = EventRegister1 = mongoose.model("EventRegister", EventRegister);
