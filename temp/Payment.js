const mongoose = require('mongoose');

const Payment = new mongoose.Schema({
  type: Number, // General DelCard, conclave etc
  paid: Boolean,
  amount: Number,
  order_id: String,
  timestamp: String,
});

module.exports = Payment1 = mongoose.model("Payment", Payment);