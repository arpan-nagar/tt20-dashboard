const mongoose = require("mongoose");
const EventRegister1 = require('./EventRegistration');
const Payment1 = require("./Payment")
const BoughtCards1 = require("./BoughtCards")
const User = new mongoose.Schema({
  delid: String,
  name: String,
  regno: String,
  mobile: String,
  email: String,
  password: String,
  qr: String,
  collname: String,
  isMAHE: Boolean,
  token: String,
  holderName: String,
  bankAccountNumber: String,
  IFSCCode: String,
  bankAddress: String,
  branchName: String,
  regEvents: [EventRegister1.schema],
  delcards: [BoughtCards1.schema],
  payments: [Payment1.schema],
});

module.exports = mongoose.model("User", User);